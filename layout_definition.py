import tkinter as tk


def create_layout(window, settings, callbacks):
    left_panel = create_panel(window, settings, 0)
    right_panel = create_panel(window, settings, 1)

    parse_input_wrapper, reload_parser = callbacks

    # Create a frame for the button row
    button_frame = tk.Frame(left_panel, bg=settings["bgcolor"])
    button_frame.pack(pady=(0, 10))

    # Create a text entry field on the left panel
    input_entry = tk.Text(
        left_panel, bg="lightgrey", width=80, height=50, font=settings["text_entry_font"]
    )
    # Create a text entry field on the left panel
    input_entry.pack(padx=10, pady=10)

    # Create a text widget on the right panel to display the parsed output
    output_text = tk.Text(
        right_panel, bg="lightgrey", width=80, height=50, font=settings["text_widget_font"]
    )

    # Create a button to trigger parsing
    parse_button = tk.Button(button_frame, text="Parse", command=parse_input_wrapper)
    parse_button.pack(side="left", padx=5)

    # Create a button to reload the parser
    reload_button = tk.Button(button_frame, text="Reload Parser", command=reload_parser)
    reload_button.pack(side="left", padx=5)

    # Create a text widget on the right panel to display the parsed output
    output_text.pack(padx=10, pady=10, fill="both", expand=True)

    # Configure the text widget to make the text selectable
    output_text.configure(selectbackground="blue", selectforeground="white")
    output_text.bind("<Control-a>", lambda event: event.widget.tag_add("sel", "1.0", "end"))
    output_text.bind("<Control-A>", lambda event: event.widget.tag_add("sel", "1.0", "end"))

    return (input_entry, output_text)


# TODO Rename this here and in `create_layout`
def create_panel(window, settings, column):
    # Create the left panel
    result = tk.Frame(
        window,
        width=settings["panel_width"],
        height=settings["panel_height"],
        bg=settings["bgcolor"],
        padx=10,
        pady=10,
    )
    result.grid(row=0, column=column, sticky="nsew")
    result.grid_propagate(False)

    return result
