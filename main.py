import tkinter as tk
import importlib
import inspect
from my_parser import my_parser as parse_input_module
import json
from file_operations import load_file, save_file
from layout_definition import create_layout


def reload_modules(parent_module):
    importlib.reload(parent_module)
    module_list = [m[1] for m in inspect.getmembers(parent_module, inspect.ismodule)]
    for module in module_list:
        importlib.reload(module)
        importlib.import_module(module.__name__)


def reload_parser():
    parse_input_module = importlib.import_module("my_parser")
    reload_modules(parse_input_module)
    reload_modules(parse_input_module)
    print("Parser reloaded")


def update():
    print("Updating title!")
    window.title(f'{settings["base_title"]} - {state.get("loaded_file", "NEW")}')


def parse_input_wrapper(event=None):
    reload_parser()
    parse_input_module.parse_input(input_entry, output_text)


def increase_font_size(event=None):
    current_size = int(output_text.cget("font").split()[-1])
    new_size = current_size + 2
    input_entry.configure(font=("Courier New", new_size))
    output_text.configure(font=("Courier New", new_size))


def decrease_font_size(event=None):
    current_size = int(output_text.cget("font").split()[-1])
    new_size = max(current_size - 2, 8)
    input_entry.configure(font=("Courier New", new_size))
    output_text.configure(font=("Courier New", new_size))


def graceful_exit():
    if state["dirty"] and tk.messagebox.askyesno(  # type: ignore
        "Unsaved Changes", "Do you want to save the changes?"
    ):
        save_file(input_entry, state, state.get("loaded_file"))
    save_state()
    window.quit()


def update_dirty_flag(event=None):
    state["dirty"] = input_entry.edit_modified()


def save_state():
    state["autosave_backup"] = input_entry.get("1.0", "end-1c")
    with open("state.json", "w") as file:
        json.dump(state, file)


def save_existing_file(event=None):
    if filepath := state.get("loaded_file"):
        save_file(input_entry, state, filepath)


def load_state():
    try:
        with open("state.json", "r") as file:
            state.update(json.load(file))
            if autosave := state.get("autosave_backup"):
                input_entry.insert("1.0", autosave)
    except FileNotFoundError:
        pass


def show_right_click_menu(event):
    right_click_menu.tk_popup(event.x_root, event.y_root)


# Settings dictionary
settings = {
    "window_size": (1024, 800),
    "panel_width": 500,
    "panel_height": 400,
    "text_entry_font": ("Courier New", 12),
    "text_widget_font": ("Courier New", 12),
    "bgcolor": "darkgrey",
    "base_title": "MacroParser",
}

# Create the main window
window = tk.Tk()

# Set the window title and minimum size
window.title(settings["base_title"])
window.minsize(*settings["window_size"])
window.resizable(False, False)


# Create the menu bar
menu_bar = tk.Menu(window)
window.config(menu=menu_bar)

# Create the layout
input_entry, output_text = create_layout(window, settings, (parse_input_wrapper, reload_parser))
input_entry.bind("<<Modified>>", update_dirty_flag)


# Create the File menu
file_menu = tk.Menu(menu_bar, tearoff=0)
menu_bar.add_cascade(label="File", menu=file_menu)
file_menu.add_command(
    label="Load", command=lambda: (load_file(input_entry, state), update()), accelerator="Ctrl+O"
)

file_menu.add_command(label="Save", command=save_existing_file, accelerator="Ctrl+S")
file_menu.add_command(
    label="Save as", command=lambda: save_file(input_entry, state), accelerator="Ctrl+Shift+S"
)
file_menu.add_command(label="Save Output", command=lambda: save_file(output_text, state))


window.bind("<Control-s>", save_existing_file)

menu_bar.configure(bg=settings["bgcolor"])

# Bind the right-click event to the text widget
input_entry.bind("<Button-3>", show_right_click_menu)
output_text.bind("<Button-3>", show_right_click_menu)


def cut_text():
    input_entry.event_generate("<<Cut>>")
    output_text.event_generate("<<Cut>>")


def copy_text():
    input_entry.event_generate("<<Copy>>")
    output_text.event_generate("<<Copy>>")


def paste_text():
    input_entry.event_generate("<<Paste>>")
    output_text.event_generate("<<Paste>>")


# Create a right-click menu
right_click_menu = tk.Menu(window, tearoff=False)
right_click_menu.add_command(label="Cut", command=cut_text)
right_click_menu.add_command(label="Copy", command=copy_text)
right_click_menu.add_command(label="Paste", command=paste_text)


# Create the state dictionary
state = {"loaded_file": None, "autosave_backup": "", "dirty": False, "dynamic_reload": False}

# Load the state
load_state()

update()

# Save the state on window close
window.protocol("WM_DELETE_WINDOW", graceful_exit)

# Start the application
window.mainloop()
