import tkinter as tk
import os

import my_parser.parse_macros as pm

DEBUG = os.environ.get("DEBUG", False)


def debug(args):
    if not DEBUG:
        return
    from pprint import pprint

    pprint(*args)


def parse_input(input_entry, output_text: tk.Text):
    # Get the input from the text entry field
    input_text = input_entry.get("1.0", "end-1c")

    # Split the input into lines
    lines = input_text.split("\n")

    # Initialize variables for labels, parsed lines, and macros
    labels = []
    parsed_lines = []
    lines, macros = pm.find_macros(lines)
    # Iterate over the lines

    debug((lines, macros))
    line_num = 1
    label_line_num = None
    for line in lines:
        if line.strip() == "":
            continue

        if pm.is_macro_start(line) or line.startswith(pm.MACRO_END):
            # Prevent normal processing for macro defining lines
            raise pm.ParserError("A leftover raw macro line")
        elif pm.MACRO_BEGIN in line:
            for line_num, m_line in pm.yield_macro_lines(line, macros, line_num):
                parsed_lines.append((line_num, m_line))
        # Check if the line ends with a colon
        elif line.endswith(":"):
            # Treat it as a label and store the label text and line number
            label_text = line[:-1].strip()  # Remove the colon and leading/trailing whitespace
            labels.append((line_num, label_text))
            label_line_num = line_num
        else:
            # Treat it as a parsed line and store the line number and parsed text
            if label_line_num is None:
                parsed_lines.append((line_num, line))
            else:
                parsed_lines.append((label_line_num, line))
            line_num += 1
            label_line_num = None

    output = ""
    if labels:
        output = "Labels:\n\t" + "\t".join(
            f"{label_text} -> {label_line_num}\n" for label_line_num, label_text in labels
        )
    for macro_name, macro_lines in macros.items():
        output += f"{macro_name}:\n"
        for m_line in macro_lines:
            output += f"\t{m_line}\n"

    line_index = 1
    for line_num, line_text in parsed_lines:
        print((line_num, line_text))
        new_line_index, line_text = pm.map_label_to_address(line_text, labels, line_index)
        # if new_line_index > line_index + 1:
        #     output += f"{line_text}\n"
        # else:
        output += f"{line_index} {line_text}\n"
        line_index = new_line_index

    # Output the result on the right panel
    output_text.delete("1.0", "end")
    output_text.insert("1.0", output)
