# from my_parser.my_parser import ParserError

MACRO_BEGIN = "#macro"
MACRO_END = "#endmacro"


class ParserError(Exception):
    pass


def yield_macro_lines(line, macros, line_num):
    # Replace macros in the line with their corresponding values

    for macro_name, macro_lines in macros.items():
        if macro_name in line:
            line_num += len(macro_lines)
            for m_line in macro_lines:
                yield (line_num, m_line)


def map_label_to_address(line, labels, line_index):
    # Replace label references with line numbers
    for label_line_num, label_text in labels:
        label_ref = f"{label_text.strip()}"
        if line.endswith(label_ref):
            line = line.replace(label_ref, f"{label_line_num}")
    line_index = line_index + 1
    return line_index, f"{line}"


def get_macro_name_and_first_line(line):
    # Extract the macro name and content
    macro_name, macro_content = line.split(":", 2)
    return macro_name.strip(), macro_content.strip()


def is_macro_start(line):
    return line.startswith(MACRO_BEGIN) and ":" in line


def find_macros(lines):
    line_number = 1
    macros = {}
    out_lines = []
    line_buffer = []
    in_macro = False
    current_macro_name = ""
    while line_number <= len(lines):
        line = lines[line_number - 1]
        if is_macro_start(line):
            macro_name, macro_line = get_macro_name_and_first_line(line)
            macros[macro_name] = macros.get(macro_name, [macro_line])
            if in_macro:
                # open buffer, flush
                out_lines.extend(line_buffer)
                line_buffer.clear()
            in_macro = True
            current_macro_name = macro_name
        elif line.startswith(MACRO_END):
            if in_macro:
                macros[current_macro_name].extend(line_buffer)
                line_buffer.clear()
                in_macro = False
            else:
                raise ParserError("Should never go here")
        elif in_macro:
            line_buffer.append(line)
        else:
            out_lines.append(line)
        line_number += 1
    # flush buffer
    out_lines.extend(line_buffer)
    return out_lines, macros


if __name__ == "__main__":
    lines, macros = find_macros(
        [
            "Line 1",
            "Line 2",
            "Label 1:",
            "#macro1: This is the content of macro1",
            "Line 3",
            "#macro2: This is the content of macro2",
            "starthere",
            "some",
            "label:",
            "#macros:a",
            "#macro:single_line_macro",
            "#macro goes multiline: electric boogaloo",
            "keep",
            "going",
            "until",
            "#endmacro",
            "and",
            "#macrom2:",
            "#endmacro",
            "good",
            "#macrom3:",
            "oneline",
            "#unmacro",
            "#macro call",
            "#endmacro",
            "space",
            "#macros end to end:",
            "#macros back to back:",
            "#macrolast:",
            "has no lines",
            "#macro1",
            "#macro2 called",
        ]
    )
    from pprint import pprint

    pprint(lines)
    pprint(macros)
