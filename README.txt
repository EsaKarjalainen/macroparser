Here are user instructions for the macro parser, written in the syntax of the macro parser itself:

Start:

Welcome to the Macro Parser!

#macro1: This is a simple macro example
#macro2: This is another macro example

To use the Macro Parser, follow these steps:

Input your code in the text entry field.
Use labels to mark specific points in your code. To create a label, end the line with a colon (":").
Example:
	Label1:
Define macros to reuse code snippets. To create a macro, use the #macro keyword followed by a unique name and a colon (":"). Then, provide the content of the macro on the following lines.
Example:
	#macro1:
	This is the content of macro1
	Line 2 of macro1
	#endmacro
Use macros in your code by referencing their names. To reference a macro, simply type the macro name in your code.
Example:
	#macro1

The Macro Parser will expand the macros and replace the macro references with their corresponding content.
The Macro Parser will also replace label references with the line numbers of the corresponding labels.
The parsed code will be displayed in the output panel on the right.
End:
---
Labels:
	Here are user instructions for the macro parser, written in the syntax of the macro parser itself -> 1
	Start -> 1
	To use the Macro Parser, follow these steps -> 2
	Example -> 4
	Label1 -> 4
	Example -> 4
	Example -> 9
	End -> 13
#macro1:
	This is a simple macro example
#macro2:
	This is another macro example
1 Welcome to the Macro Parser!
2 Input your code in the text entry field.
3 Use labels to mark specific points in your code. To create a label, end the line with a colon (":").
4 This is a simple macro example
5 	This is the content of macro1
6 	Line 2 of macro1
7 	#endmacro
8 Use macros in your code by referencing their names. To reference a macro, simply type the macro name in your code.
9 This is a simple macro example
10 The Macro Parser will expand the macros and replace the macro references with their corresponding content.
11 The Macro Parser will also replace label references with the line numbers of the corresponding labels.
12 The parsed code will be displayed in the output panel on the right.
