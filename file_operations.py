from tkinter import filedialog, Text


def load_file(input_entry: Text, state):
    if file_path := filedialog.askopenfilename(filetypes=[("Text Files", "*.txt")]):
        with open(file_path, "r") as file:
            input_entry.delete("1.0", "end")
            input_entry.insert("1.0", file.read())
        state["loaded_file"] = file_path
        state["autosave_backup"] = input_entry.get("1.0", "end-1c")


def save_file(input_entry: Text, state, file_path=None):
    if not file_path:
        file_path = filedialog.asksaveasfilename(
            defaultextension=".txt", filetypes=[("Text Files", "*.txt")]
        )
    if file_path:
        with open(file_path, "w") as file:
            file.write(input_entry.get("1.0", "end-1c"))
        state["loaded_file"] = file_path
        state["autosave_backup"] = input_entry.get("1.0", "end-1c")
        state["dirty"] = False
        input_entry.edit_modified(False)
